# EOH.IoTCard

#### 介绍
对接各大中国电信、中国联通、中国移动、UPIoT物联卡开发平台SDK

##### 安装
使用 NuGet 包管理器安装：

##### 使用示例
初始化

##### 贡献
欢迎贡献代码、报告问题和提供建议！请在 Gitee/GitHub 项目中提交 Issue 或 Pull Request。

##### 许可
本项目基于 MIT 许可进行分发和使用。详细信息请查阅 LICENSE 文件。

##### 作者
作者：荣少<br/>
邮箱：ligengrong@hotmail.com<br/>
QQ群：7405133

##### 🏅开源地址
[![Gitee](https://shields.io/badge/Gitee-https://gitee.com/LeifRong/EOH.IoTCard-green?logo=gitee&style=flat&logoColor=red)](https://gitee.com/LeifRong/EOH.IoTCard)  <br/>

### 🧧 赞赏作者

	你的赞赏就是我前进的动力🏃，
	如果EOH.IoTCard给你带来了不少便利，
	那就请作者喝杯咖啡☕吧。
![](https://gitee.com/LeifRong/EOH.Sms/raw/master/img/pay.jpg)
