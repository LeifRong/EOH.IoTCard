﻿namespace EOH.IoTCard.UPIoT
{
    public class UPIoTOptions
    {
        public string? ApiKey { set; get; }
        public string? ApiSecret { get; set; }
    }
}
