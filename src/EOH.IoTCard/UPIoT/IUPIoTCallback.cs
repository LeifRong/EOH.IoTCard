﻿using System.Threading.Tasks;

namespace EOH.IoTCard.UPIoT
{
    public interface IUPIoTCallback<T>
    {
        /// <summary>短信回调</summary>
        Task<T> SmsCallback(string clientId, SmsModel? model);
        /// <summary>API接口停复机操作回调</summary>
        Task<T> StopRestartOperationCallback(string clientId, StopRestartOperationModel model);
        /// <summary>单卡流量预警回调</summary>
        Task<T> TrafficWarningCallback(string clientId);
        /// <summary>流量池流量预警回调</summary>
        Task<T> TrafficPoolTrafficWarningCallback(string clientId);
    }

    public class SmsModel
    {
        /// <summary>短信下行回调</summary>
        public int code { get; set; }
        /// <summary>附加说明</summary>
        public string? msg { get; set; }
        /// <summary>该短信在平台的唯一标识</summary>
        public string? sms_id { get; set; }
        /// <summary>inbox</summary>
        public string? type { get; set; }
        /// <summary>物联卡号</summary>
        public string? msisdn { get; set; }
        /// <summary>短信内容</summary>
        public string? content { get; set; }
    }
    public class StopRestartOperationModel
    {
        /// <summary>物联卡号</summary>
        public string? msisdn { get; set; }
        /// <summary>客户传入的操作单号</summary>
        public string? callback_no { get; set; }
        /// <summary>当前卡状态：正使用、停机...</summary>
        public string? account_status { get; set; }
        /// <summary>短信下行回调</summary>
        public int code { get; set; }
        /// <summary>附加说明</summary>
        public string? msg { get; set; }
    }
    public class TrafficPoolTrafficWarningModel
    {
        /// <summary>附加说明</summary>
        public string? msg { get; set; }
    }
    public class TrafficWarningModel : TrafficPoolTrafficWarningModel { }
}