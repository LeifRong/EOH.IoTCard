﻿using System.Collections.Generic;
using System.Net.Http;

namespace EOH.IoTCard.UPIoT.Res
{
    /// <summary>计费组列表</summary>
    /// <remarks>/billing_group</remarks>
    public class BillingGroupRequest : UPIoTRequest<UPIoTResponse<BillingGroupResponse>>
    {
        public override string? Url => "/billing_group";
        public override HttpMethod Method => HttpMethod.Get;
    }
    public class BillingGroupResponse
    {
        public IList<BillingGroupRow> rows { get; set; }

        public class BillingGroupRow
        {
            /// <summary>运营商</summary>
            public string? carrier { get; set; }
            /// <summary>计费组代码</summary>
            public string? bg_code { get; set; }
            /// <summary>流量池代码</summary>
            public string? vup_code { get; set; }
            /// <summary>套餐名称</summary>
            public string? name { get; set; }
            /// <summary>套餐大小</summary>
            public string? data_plan { get; set; }
        }
    }
}
