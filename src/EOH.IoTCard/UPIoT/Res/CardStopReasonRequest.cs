﻿using System.Net.Http;

namespace EOH.IoTCard.UPIoT.Res
{
    /// <summary>机卡分离停机情况单卡查询接口</summary>
    /// <remarks>card/msisdn|iccid|imsi/stop_reason 根据物联卡号（msisdn/iccid/imsi），返回机卡分离停机情况，目前只支持部分中国移动和部分中国电信的卡</remarks>
    public class CardStopReasonRequest : UPIoTRequest<UPIoTResponse>
    {
        string? _Url;
        public override string? Url => _Url;
        public override HttpMethod Method => HttpMethod.Get;
        /// <summary>msisdn/iccid/imsi,任意一个</summary>
        public string? QueryKey { set => _Url = $"/card/{value}/stop_reason"; }
    }
}
