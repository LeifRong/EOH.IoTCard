﻿using System.Collections.Generic;

namespace EOH.IoTCard.UPIoT.Res
{
    /// <summary>批量查询卡信息</summary>
    /// <remarks>/batch/card/info</remarks>
    public class BatchCardInfoRequest : UPIoTRequest<BatchCardInfoModel, UPIoTResponse<IList<CardResponse>>>
    {
        public override string? Url => "/batch/card/info";
    }

    public class BatchCardInfoModel
    {
        public IList<string>? msisdns { get; set; }
    }
}
