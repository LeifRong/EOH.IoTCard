﻿using System;
using System.Net.Http;

namespace EOH.IoTCard.UPIoT.Res
{
    /// <summary>物联卡信息</summary>
    /// <remarks>/card/msisdn|iccid|imsi</remarks>
    public class CardRequest : UPIoTRequest<UPIoTResponse<CardResponse>>
    {
        string? _Url = "/card/{0}";
        public override string? Url => _Url;
        public override HttpMethod Method => HttpMethod.Get;
        /// <summary>msisdn/iccid/imsi,任意一个</summary>
        public string? QueryKey { set => _Url = $"/card/{value}"; }
    }

    public class CardResponse
    {
        /// <summary>msisdn</summary>
        public string? msisdn { get; set; }
        /// <summary>iccid</summary>
        public string? iccid { get; set; }
        /// <summary>imsi</summary>
        public string? imsi { get; set; }
        /// <summary>imsi2</summary>
        public string? imsi2 { get; set; }
        /// <summary>运营商</summary>
        public string? carrier { get; set; }
        /// <summary>短信端口号</summary>
        public string? sp_code { get; set; }
        /// <summary>套餐大小</summary>
        public string? data_plan { get; set; }
        /// <summary>计费结束日期</summary>
        public DateTime expiry_date { get; set; }
        /// <summary>当月流量</summary>
        public string? data_usage { get; set; }
        /// <summary>卡状态</summary>
        public string? account_status { get; set; }
        /// <summary>激活/未激活</summary>
        public bool active { get; set; }
        /// <summary>测试期起始日期</summary>
        public DateTime test_valid_date { get; set; }
        /// <summary>沉默期起始日期</summary>
        public DateTime silent_valid_date { get; set; }
        /// <summary>出库日期</summary>
        public DateTime outbound_date { get; set; }
        /// <summary>激活日期</summary>
        public DateTime? active_date { get; set; }
        /// <summary>是否支持短信</summary>
        public bool support_sms { get; set; }
        /// <summary>剩余流量</summary>
        public string? data_balance { get; set; }
        /// <summary>测试期已用流量</summary>
        public string? test_used_data_usage { get; set; }
        /// <summary>sim卡类型</summary>
        public string? sim_type { get; set; }
        /// <summary>是否是累计卡，boolean类型</summary>
        public bool accumulated { get; set; }
        /// <summary>计费组code</summary>
        public string? code { get; set; }
        /// <summary>总流量</summary>
        public string? data_traffic_amount { get; set; }
        /// <summary>计费起始日期</summary>
        public DateTime valid_date { get; set; }
        /// <summary>超出用量</summary>
        public int out_data_usage { get; set; }
        /// <summary>标签</summary>
        public string? customize_comment { get; set; }
        /// <summary>销户月份</summary>
        public string? inactive_month { get; set; }
    }
}
