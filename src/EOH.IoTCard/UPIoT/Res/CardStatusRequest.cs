﻿using System.Net.Http;

namespace EOH.IoTCard.UPIoT.Res
{
    /// <summary>物联卡状态</summary>
    /// <remarks>/card/msisdn|iccid|imsi/status</remarks>
    public class CardStatusRequest : UPIoTRequest<UPIoTResponse<CardStatusResponse>>
    {
        string? _Url;
        public override string? Url => _Url;
        public override HttpMethod Method => HttpMethod.Get;
        /// <summary>msisdn/iccid/imsi,任意一个</summary>
        public string? QueryKey { set => _Url = $"/card/{value}/status"; }
    }

    public class CardStatusResponse
    {
        /// <summary>工作状态</summary>
        public int gprs_status { get; set; }
        /// <summary></summary>
        public string? gprs_status_msg { get; set; }
        /// <summary>开关机状态</summary>
        public int power_status { get; set; }
        /// <summary></summary>
        public string? power_status_msg { get; set; }
        /// <summary>网络接入类型</summary>
        public string? RAT { get; set; }
        /// <summary>接入IP地址,  (部分有，部分没有)</summary>
        public string? IP { get; set; }
        /// <summary>APN部分有，部分没有</summary>
        public string? APN { get; set; }
        /// <summary>无线接入模式(3G/4G) (目前只有电信卡有此字段, 查询失败此字段为`未知`)</summary>
        public string? net_model { get; set; }
        /// <summary>最后一次下线时间 (目前只有电信卡有此字段, 查询失败此字段为`未知`)</summary>
        public string? stop_time { get; set; }
        /// <summary最后一次上线时间(gprs_status 为`工作状态`或查询失败该值为`未知`) (目前只有电信卡有此字段)</summary>
        public string? start_time { get; set; }
    }
}
