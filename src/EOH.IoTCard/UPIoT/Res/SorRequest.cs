﻿namespace EOH.IoTCard.UPIoT.Res
{
    /// <summary>停复机相关接口</summary>
    /// <remarks>
    /// /sor
    /// 物联卡停机、复机。部分中国移动物联网卡不支持停复机； 
    /// 本平台支持操作移动物联卡单日单张卡停机或复机不超过2次，电信、联通物联卡不限次数； 
    /// 平台停复机操作功能，有一定时延，请勿频繁操作停复机；如有疑问请联系客户经理或客服
    /// 由于停复机，有可能不成功，需要对于回调接口判断是否成功
    /// </remarks>
    public class SorRequest : UPIoTRequest<SorModel, UPIoTResponse>
    {
        public override string? Url => "/sor";
    }
    public class SorModel
    {
        /// <summary>number</summary>
        public string? number { get; set; }
        /// <summary>回调任务单号</summary>
        public string? callback_no { get; set; }
        /// <summary>停复机类型，分为两种类型。00 表示复机， 01表示停机</summary>
        public string? type { get; set; }

    }
}
