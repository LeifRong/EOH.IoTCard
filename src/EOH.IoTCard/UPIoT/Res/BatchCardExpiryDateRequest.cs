﻿using System;
using System.Collections.Generic;
using System.Net.Http;

namespace EOH.IoTCard.UPIoT.Res
{
    /// <summary>本月到期卡详情查询</summary>
    /// <remarks>/batch/card/expiry_date</remarks>
    public class BatchCardExpiryDateRequest : UPIoTRequest<UPIoTResponse<BatchCardExpiryDateResponse>>
    {
        public override string? Url => $"/batch/card/expiry_date";
        public override HttpMethod Method => HttpMethod.Get;
    }

    public class BatchCardExpiryDateResponse
    {
        public int page { get; set; }
        public int per_page { get; set; }
        public int num_pages { get; set; }
        public IList<BatchCardExpiryDateDetail>? rows { get; set; }

        public class BatchCardExpiryDateDetail
        {
            public string? iccid { get; set; }
            public string? msisdn { get; set; }
            public string? imsi { get; set; }
            public DateTime? expiry_date { get; set; }
        }
    }
}
