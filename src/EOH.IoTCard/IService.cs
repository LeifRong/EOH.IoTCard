﻿using System.Threading.Tasks;

namespace EOH.IoTCard
{
    /// <summary>服务接口</summary>
    public interface IService
    {
        /// <summary>执行函数</summary>
        /// <typeparam name="TResponse"></typeparam>
        /// <param name="request"></param>
        /// <returns></returns>
        Task<TResponse> Execute<TResponse>(IRequest<TResponse> request);
        /// <summary>
        /// 执行函数
        /// </summary>
        /// <typeparam name="TModel">请求参数实体</typeparam>
        /// <typeparam name="TResponse">返回参数实体</typeparam>
        /// <param name="request">请求实体</param>
        /// <returns></returns>
        Task<TResponse> Execute<TModel, TResponse>(IRequest<TModel, TResponse> request);
    }
}
