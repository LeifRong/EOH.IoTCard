﻿using System.Collections.Generic;

namespace EOH.IoTCard.CMCC.Res
{
    /// <summary>CMIOT_API25S04-单卡状态查询</summary>
    /// <remarks>ec/query/sim-status</remarks>
    public class QuerySimStatusRequest : CmccRequest<QuerySimStatusModel, CmccResponse<IList<QuerySimStatusResponse>>>
    {
        public override string? Url => "/v5/ec/query/sim-status";
    }

    public class QuerySimStatusModel
    {
        public string? msisdn { get; set; }
        public string? iccid { get; set; }
        public string? imsi { get; set; }
    }

    public class QuerySimStatusResponse
    {
        /// <summary>当开卡平台为OneLink-PB时：00-正常、01-单向停机、02-停机、03-预销号、05-过户、06-休眠、07-待激活、99-号码不存在；当开卡平台为OneLink-CT时：1：待激活、2：已激活、4：停机、6：可测试、7：库存、8：预销户</summary>
        public string? cardStatus { get; set; }

        /// <summary>lastChangeDate</summary>
        public string? lastChangeDate { get; set; }
    }
}