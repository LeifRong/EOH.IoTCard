﻿using System;
using System.Collections.Generic;

namespace EOH.IoTCard.CMCC.Res
{
    /// <summary>CMIOT_API23S00-单卡基本信息查询</summary>
    /// <remarks>ec/query/sim-basic-info</remarks>
    public class QuerySimBasicInfoRequest : CmccRequest<QuerySimStatusModel, CmccResponse<IList<QuerySimBasicInfoResponse>>>
    {
        public override string? Url => "/v5/ec/query/sim-basic-info";
    }
    public class QuerySimBasicInfoResponse : QuerySimStatusModel
    {
        public DateTime? activeDate { get; set; }
        public DateTime? openDate { get; set; }
    }
}
