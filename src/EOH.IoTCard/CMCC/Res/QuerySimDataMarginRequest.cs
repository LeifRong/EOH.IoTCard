﻿using System.Collections.Generic;

namespace EOH.IoTCard.CMCC.Res
{
    public class QuerySimDataMarginRequest : CmccRequest<QuerySimStatusModel, CmccResponse<IList<QuerySimDataMarginResponse>>>
    {
        public override string? Url => "/v5/ec/query/sim-data-margin";
    }
    public class QuerySimDataMarginResponse
    {
        public IList<AccmMargin>? accmMarginList { get; set; }
        public class AccmMargin
        {
            /// <summary>资费ID</summary>
            public string? offeringId { get; set; }
            /// <summary>资费名称</summary>
            public string? offeringName { get; set; }
            /// <summary>总量，单位：Kb</summary>
            public string? totalAmount { get; set; }
            /// <summary>使用量，单位：Kb</summary>
            public string? useAmount { get; set; }
            /// <summary>剩余量，单位：Kb</summary>
            public string? remainAmount { get; set; }
            /// <summary>APN名称，无值则返回为" "</summary>
            public string? apnName { get; set; }
            /// <summary>pccServiceCode编码 无值则返回为" "</summary>
            public string? pccCode { get; set; }
        }
    }
}
