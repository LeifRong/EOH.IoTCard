﻿using System.Collections.Generic;

namespace EOH.IoTCard.CMCC.Res
{
    public class QuerySimBatchResultRequest : CmccRequest<QuerySimBatchResultModel, CmccResponse<IList<QuerySimBatchResultResponse>>>
    {
        public override string? Url => "/v5/ec/query/sim-batch-result";
    }
    public class QuerySimBatchResultModel
    {
        /// <summary>物联卡批量处理的任务流水号</summary>
        public string? jobId { get; set; }
    }
    public class QuerySimBatchResultResponse
    {
        /// <summary>任务状态： 0：待处理 1：处理中 2：处理完成 3：包含有处理失败记录的处理完成 4：处理失败</summary>
        public string? jobStatus { get; set; }
        public IList<Result>? resultList { get; set; }
        public class Result
        {
            /// <summary>查询状态，0-成功，非0-失败</summary>
            public string? status { get; set; }
            /// <summary>错误信息，错误码对应的错误描述，参考错误码列表</summary>
            public string? message { get; set; }
            /// <summary>返回号码类型： 1：msisdn</summary>
            public string? resultType { get; set; }
            /// <summary>返回号码</summary>
            public string? resultId { get; set; }
        }
    }
}
