﻿using System.Collections.Generic;

namespace EOH.IoTCard.CMCC.Res
{
    /// <summary>CMIOT_API23E06-集团群组信息查询</summary>
    /// <remarks>ec/query/group-info</remarks>
    public class QueryGroupInfoRequest : CmccRequest<QueryGroupInfoModel, CmccResponse<IList<QueryGroupInfoResponse>>>
    {
        public override string? Url => "/v5/ec/query/group-info";
    }
    public class QueryGroupInfoModel
    {
        /// <summary>每页查询的数目，不超过50</summary>
        public string? pageSize { get; set; }
        /// <summary>开始页，从1开始</summary>
        public string? startNum { get; set; }
    }
    public class QueryGroupInfoResponse
    {
        /// <summary>群组总数</summary>
        public string? totalCount { get; set; }
        /// <summary>群组列表</summary>
        public IList<GroupInfo>? groupList { get; set; }
        public class GroupInfo
        {
            /// <summary>群组ID</summary>
            public string? groupId { get; set; }
            /// <summary>群组名称</summary>
            public string? groupName { get; set; }
            /// <summary>群组主资费ID</summary>
            public string? offeringId { get; set; }
            /// <summary>群组主资费名称</summary>
            public string? offeringName { get; set; }
        }
    }
}
