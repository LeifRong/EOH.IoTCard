﻿using System.Collections.Generic;

namespace EOH.IoTCard.CMCC.Res
{
    /// <summary>CMIOT_API23R07-目录节点实时查询</summary>
    /// <remarks>ec/query/categories</remarks>
    public class QueryCategoriesRequest : CmccRequest<QueryCategoriesModel, CmccResponse<IList<QueryCategoriesResponse>>>
    {
        public override string? Url => "/v5/ec/query/categories";
    }
    public class QueryCategoriesModel
    {
        /// <summary>查询目录节点应用场景（1：集团客户资费；2: 集团群组附属资费；3: 个人(集团用户)附属资费）</summary>
        public string? queryScenes { get; set; }
    }
    public class QueryCategoriesResponse
    {
        /// <summary>目录节点信息列表，包含catalogId、categoryId、categoryName、categoryDesc、categoryOrder、categoryParent</summary>
        public IList<CategoryInfo>? categoryList { get; set; }
        public class CategoryInfo
        {
            /// <summary>目录标识</summary>
            public string? catalogId { get; set; }
            /// <summary>节点标识</summary>
            public string? categoryId { get; set; }
            /// <summary>节点名称</summary>
            public string? categoryName { get; set; }
            /// <summary>节点描述</summary>
            public string? categoryDesc { get; set; }
            /// <summary>节点操作员显示顺序</summary>
            public string? categoryOrder { get; set; }
            /// <summary>节点父节点</summary>
            public string? categoryParent { get; set; }
        }
    }
}
