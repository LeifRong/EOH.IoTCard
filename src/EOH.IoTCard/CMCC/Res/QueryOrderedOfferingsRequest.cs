﻿using System;
using System.Collections.Generic;

namespace EOH.IoTCard.CMCC.Res
{
    /// <summary>CMIOT_API23R00-资费订购实时查询</summary>
    /// <remarks>ec/query/ordered-offerings</remarks>
    public class QueryOrderedOfferingsRequest : CmccRequest<QueryOrderedOfferingsModel, CmccResponse<IList<QueryOrderedOfferingsResponse>>>
    {
        public override string? Url => "/v5/ec/query/ordered-offerings";
    }
    public class QueryOrderedOfferingsModel
    {
        /// <summary>查询场景标识类型（1：客户接入类型；2：群组接入类型；3：sim接入类型）</summary>
        public string? queryType { get; set; }
        /// <summary>接入群组编号，queryType取值为2时传入</summary>
        public string? groupId { get; set; }
        /// <summary>物联卡号码，queryType取值为3时传入</summary>
        public string? msisdn { get; set; }
    }
    public class QueryOrderedOfferingsResponse
    {
        public IList<OfferingInfo>? offeringInfoList { get; set; }
        public class OfferingInfo
        {
            /// <summary>资费ID</summary>
            public string? offeringId { get; set; }
            /// <summary>资费名称</summary>
            public string? offeringName { get; set; }
            /// <summary>生效时间</summary>
            public DateTime? effectiveDate { get; set; }
            /// <summary>失效时间</summary>
            public DateTime? expiriedDate { get; set; }
            /// <summary>APN名称</summary>
            public string? apnName { get; set; }
        }
    }
}
