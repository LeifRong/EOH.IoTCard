﻿using System.Collections.Generic;

namespace EOH.IoTCard.CMCC.Res
{
    /// <summary>CMIOT_API25U03-物联卡单月GPRS流量使用量批量查询 /ec/query/sim-data-usage-monthly/batch</summary>
    public class QuerySimDataUsageMonthlyRequest : CmccRequest<QuerySimDataUsageMonthlyModel, CmccResponse<IList<QuerySimDataUsageMonthlyResponse>>>
    {
        public override string? Url => "/v5/ec/query/sim-data-usage-monthly/batch";
    }
    public class QuerySimDataUsageMonthlyModel
    {
        /// <summary>所查询的物联卡号码，最长13位数字，举例：14765004176。批量查询多个号码用下划线分隔。例如：xxxx_xxxx_xxxx</summary>
        public string? msisdns { get; set; }
        /// <summary>集成电路卡识别码，IC卡的唯一识别号码，共有20位字符组成，举例：898600D6991330004146。批量查询多个号码用下划线分隔。例如：xxxx_xxxx_xxxx</summary>
        public string? iccids { get; set; }
        /// <summary>国际移动用户识别码，其总长度不超过15位，使用0~9的数字，举例：460079650004176。批量查询多个号码用下划线分隔。例如：xxxx_xxxx_xxxx</summary>
        public string? imsis { get; set; }
        /// <summary>查询最近6个月中的某月，其中本月数据截止为前一天，日期格式为yyyyMM（每月1日不支持查询本月）</summary>
        public string? queryDate { get; set; }
    }
    public class QuerySimDataUsageMonthlyResponse
    {
        /// <summary>查询的月用量统计截止时间（该时间代表系统抽取时间，如果因为离线话单导致未抽取的数据部分将无法统计，故该数据只能进行统计分析，无法支撑精准对账） 查询历史月份时为该月最后一天。 查询当月时，如果前一日数据已统计完则为前一日；如果前一日数据未统计完则为前二日。 时间格式为：yyyyMMDD</summary>
        public string? deadLine { get; set; }
        /// <summary>流量累积量值，单位：KB返回””时，表示卡未产生用量或未订购套餐PB号段为截至前一天24点流量，CT号段为实时流量</summary>
        public IList<DataAmount>? dataAmountList { get; set; }
        public class DataAmount : QuerySimStatusModel
        {
            public string? dataAmount { get; set; }
            public IList<ApnDataAmount> apnDataAmountList { get; set; }

            public class ApnDataAmount
            {
                /// <summary>Apn名称</summary>
                public string? apnName { get; set; }
                /// <summary>Apn使用量（KB）</summary>
                public string? apnDataAmount { get; set; }
            }
        }
    }
}
