﻿using System.Collections.Generic;

namespace EOH.IoTCard.CMCC.Res
{
    public class ChangeSimStatusBatchRequest : CmccRequest<ChangeSimStatusBatchModel, CmccResponse<IList<ChangeSimStatusBatchResponse>>>
    {
        public override string? Url => "/v5/ec/change/sim-status/batch";
    }
    public class ChangeSimStatusBatchModel : QuerySimStatusModel
    {
        /// <summary>物联卡号，最长13位数字，举例：14765004176。批量查询多个号码用下划线分隔。例如：xxxx_xxxx_xxxx；最少10个，最多100个</summary>
        public string? msisdns { get; set; }
        /// <summary>操作类型： 1 可测试-&gt;库存； 2可测试-&gt;待激活； 3可测试-&gt;已激活； 4库存-&gt;待激活 5库存-&gt;已激活 6 待激活-&gt;库存 7待激活-&gt;已激活 8 待激活-&gt;已停机（暂不支持） 9已激活-&gt;已停机 10已停机-&gt;待激活（暂不支持） 11 已停机-&gt;已激活</summary>
        public int? operType { get; set; }
        /// <summary>停复机原因：在operType为9或11时，原因必传 01：主动停复机</summary>
        public string? reason { get; set; }
    }

    public class ChangeSimStatusBatchResponse
    {
        /// <summary>订单编号</summary>
        public string? jobId { get; set; }
    }
}
