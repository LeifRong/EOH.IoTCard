﻿using System.Collections.Generic;

namespace EOH.IoTCard.CMCC.Res
{
    /// <summary>CMIOT_API23S04-单卡实时使用终端IMEI查询</summary>
    /// <remarks>/v5/ec/query/sim-imei</remarks>
    public class QuerySimImeiRequest : CmccRequest<QuerySimStatusModel, CmccResponse<IList<QuerySimImeiResponse>>>
    {
        public override string? Url => "/v5/ec/query/sim-imei";
    }

    public class QuerySimImeiResponse
    {
        public string? imei { get; set; }
    }
}