﻿using System.Collections.Generic;

namespace EOH.IoTCard.CMCC.Res
{
    public class ChangeSimStatusRequest : CmccRequest<QuerySimStatusModel, CmccResponse<IList<QuerySimBasicInfoResponse>>>
    {
        public override string? Url => "/v5/ec/change/sim-status";
    }
    public class ChangeSimStatusModel : QuerySimStatusModel
    {
        /// <summary>0:申请停机(已激活转已停机) 1:申请复机(已停机转已激活) 2:库存转已激活 3:可测试转库存 4:可测试转待激活 5:可测试转已激活 6:待激活转已激 8:待激活转库存 9:库存转待激活</summary>
        public int? operType { get; set; }
    }

    public class ChangeSimStatusResponse : QuerySimStatusModel
    {
        /// <summary>订单编号</summary>
        public string? orderNum { get; set; }
    }
}
