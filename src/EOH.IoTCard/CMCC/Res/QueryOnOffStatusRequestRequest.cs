﻿using System.Collections.Generic;

namespace EOH.IoTCard.CMCC.Res
{
    /// <summary>CMIOT_API25M00-单卡开关机状态实时查询</summary>
    /// <remarks>/v5/ec/query/on-off-status</remarks>
    public class QueryOnOffStatusRequest : CmccRequest<QuerySimStatusModel, CmccResponse<IList<QueryOnOffStatusResponse>>>
    {
        public override string? Url => "/v5/ec/query/on-off-status";
    }
    public class QueryOnOffStatusResponse
    {
        public int? status { get; set; }
    }
}
