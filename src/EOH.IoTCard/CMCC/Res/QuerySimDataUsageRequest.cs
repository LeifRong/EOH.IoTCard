﻿using System.Collections.Generic;

namespace EOH.IoTCard.CMCC.Res
{
    /// <summary>CMIOT_API25U04-单卡本月流量累计使用量查询 /ec/query/sim-data-usage</summary>
    public class QuerySimDataUsageRequest : CmccRequest<QuerySimStatusModel, CmccResponse<IList<QuerySimDataUsageResponse>>>
    {
        public override string? Url => "/v5/ec/query/sim-data-usage";
    }
    public class QuerySimDataUsageResponse
    {
        /// <summary>流量累积量值，单位：KB返回””时，表示卡未产生用量或未订购套餐PB号段为截至前一天24点流量，CT号段为实时流量</summary>
        public string? dataAmount { get; set; }
        public IList<ApnUseAmount>? apnUseAmountList { get; set; }
        public class ApnUseAmount
        {
            /// <summary>APN名称 若为PB卡则无此参数返回</summary>
            public string? apnName { get; set; }
            /// <summary>APN数据累计使用量，单位：KB 若为PB卡则无此参数返回</summary>
            public string? apnUseAmount { get; set; }
            /// <summary>总量，单位：Kb</summary>
            public IList<PccCodeUseAmount>? pccCodeUseAmountList { get; set; }
     
            public class PccCodeUseAmount
            {
                /// <summary>pccServiceCode编码 若为PB卡则无此参数返回</summary>
                public string? pccCode { get; set; }
                /// <summary>该pccCode数据累积量值 单位：KB 若为PB卡则无此参数返回</summary>
                public string? pccCodeUseAmount { get; set; }
            }
        }
    }
}
