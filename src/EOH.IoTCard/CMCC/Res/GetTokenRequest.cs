﻿using System;
using System.Collections.Generic;

namespace EOH.IoTCard.CMCC.Res
{
    /// <summary>token获取（仅适用于V5版本接口）</summary>
    /// <remarks>/v5/ec/get/token</remarks>
    internal class GetTokenRequest : CmccRequest<GetTokenModel, CmccResponse<IList<GetTokenResponse>>>
    {
        public override string? Url => "/v5/ec/get/token";
    }

    internal class GetTokenModel
    {
        public string? appid { set; get; }
        public string? password { get; set; }
        public int refresh { get; set; } = 1;
    }

    internal class GetTokenResponse
    {
        public string? token { get; set; }
        public int ttl { get; set; }
        public DateTime StartTime { get; set; } = DateTime.Now;

        /// <summary>是否有效</summary>
        public bool IsTokenValid
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(token) && 0 < ttl) { return DateTime.Now.AddMinutes(5) < StartTime.AddSeconds(ttl); }
                return false;
            }
        }
    }
}