﻿namespace EOH.IoTCard.CMCC
{
    public class CmccOptions
    {
        public string? AppId { set; get; }
        public string? Password { get; set; }
    }
}
