using EOH.IoTCard;
using EOH.IoTCard.UPIoT;
using EOH.IoTCard.UPIoT.Res;

using Microsoft.Extensions.DependencyInjection;

namespace Test
{
    [TestClass]
    public class UPIoTTest
    {
        static IService _UPIoTService;
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            var service = new ServiceCollection();
            service.AddHttpClient(typeof(UPIoTService).FullName, client =>
            {
                client.BaseAddress = new Uri("http://ec.upiot.net");
                client.DefaultRequestHeaders.UserAgent.ParseAdd(nameof(UPIoTService));
                client.DefaultRequestHeaders.Accept.ParseAdd("application/json; charset=utf-8");
            });
            var provider = service.BuildServiceProvider();
            _UPIoTService = new UPIoTService(provider.GetService<IHttpClientFactory>(), new UPIoTOptions
            {
                ApiKey = "",
                ApiSecret = ""
            });
        }
        [TestMethod]
        public void TestCardInfo()
        {
            var res = _UPIoTService.Execute(new CardRequest
            {
                QueryKey = "1440908283800"
            }).Result;
        }

        [TestMethod]
        public void TestBatchCardInfo()
        {
            var res = _UPIoTService.Execute(new BatchCardInfoRequest
            {
                Model = new BatchCardInfoModel { msisdns = new List<string> { "1441679983620" } }
            }).Result;
        }

        [TestMethod]
        public void TestBillingGroup()
        {
            var res = _UPIoTService.Execute(new BillingGroupRequest()).Result;
        }

        [TestMethod]
        public void TestSor()
        {
            var res = _UPIoTService.Execute(new SorRequest
            {
                Model = new SorModel
                {
                    number = "460088799803620",
                    callback_no = Guid.NewGuid().ToString(),
                    type = "00"
                }
            }).Result;
        }

        [TestMethod]
        public void TestBatchCardExpiryDate()
        {
            var res = _UPIoTService.Execute(new BatchCardExpiryDateRequest()).Result;
        }
        [TestMethod]
        public void TestCardStatus()
        {
            var res = _UPIoTService.Execute(new CardStatusRequest
            {
                QueryKey = "460085886306732"
            }).Result;
        }

        [TestMethod]
        public void TestCardStopReason()
        {
            var res = _UPIoTService.Execute(new CardStopReasonRequest
            {
                QueryKey = "89860817102340044999"
            }).Result;
        }
    }
}