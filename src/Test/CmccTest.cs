﻿using EOH.IoTCard.CMCC;
using EOH.IoTCard.CMCC.Res;

using Microsoft.Extensions.DependencyInjection;

namespace Test
{
    [TestClass]
    public class CmccTest
    {
        static CmccService _CmccService;
        [ClassInitialize]
        public static void Initialize(TestContext context)
        {
            var service = new ServiceCollection();
            service.AddHttpClient(typeof(CmccService).FullName, client =>
            {
                client.BaseAddress = new Uri("https://api.iot.10086.cn");
                client.DefaultRequestHeaders.UserAgent.ParseAdd(nameof(CmccService));
            });
            var provider = service.BuildServiceProvider();
            _CmccService = new CmccService(provider.GetService<IHttpClientFactory>(), new CmccOptions
            {
                AppId = "",
                Password = ""
            });
        }


        [TestMethod]
        public void TestCardInfo()
        {
            var res = _CmccService.Execute(new QuerySimStatusRequest { Model = new QuerySimStatusModel { msisdn = "1440242884008" } }).Result;

        }
        [TestMethod]
        public void TestQuerySimDataMargin()
        {
            var res = _CmccService.Execute(new QuerySimDataMarginRequest { Model = new QuerySimStatusModel { msisdn = "1440242884008" } }).Result;
        }
        [TestMethod]
        public void TestQueryOrderedOfferings()
        {
            var res = _CmccService.Execute(new QueryOrderedOfferingsRequest { Model = new QueryOrderedOfferingsModel { queryType = "3" } }).Result;
        }
        [TestMethod]
        public void TestQueryGroupInfo() {
            var res = _CmccService.Execute(new QueryGroupInfoRequest { Model = new QueryGroupInfoModel { pageSize = "10", startNum = "1" } }).Result;
        }
        [TestMethod]
        public void TestQueryCategories()
        {
            var res = _CmccService.Execute(new QueryCategoriesRequest { Model = new QueryCategoriesModel { queryScenes = "1" } }).Result;
            res = _CmccService.Execute(new QueryCategoriesRequest { Model = new QueryCategoriesModel { queryScenes = "2" } }).Result;
            res = _CmccService.Execute(new QueryCategoriesRequest { Model = new QueryCategoriesModel { queryScenes = "3" } }).Result;
        }
        [TestMethod]
        public void TestQuerySimDataUsageRequest()
        {
            var res = _CmccService.Execute(new QuerySimDataUsageRequest { Model = new QuerySimStatusModel { msisdn = "1440242884008" } }).Result;

        }
        [TestMethod]
        public void TestQuerySimDataUsageMonthlyRequest()
        {
            var res = _CmccService.Execute(new QuerySimDataUsageMonthlyRequest { Model = new QuerySimDataUsageMonthlyModel { msisdns = "1440242884008", queryDate="202403" } }).Result;

        }
    }
}
